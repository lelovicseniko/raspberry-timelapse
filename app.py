from src.TimeLapseCreatorApp import TimeLapseCreatorApp


def main():
    TimeLapseCreatorApp.start_process()


if __name__ == "__main__":
    main()

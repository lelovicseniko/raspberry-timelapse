# raspberry-timelapse
Downloads webcam images from FTP, creates a daily timelapse video and uploads it to YouTube.
Saves full-size video to FTP.
It needs a Google Account with permission to the YouTube upload and to e-mail sending (for monitoring).

Build and run tests:
```bash
docker-compose -f docker-compose.test.yml -p timelapse build
docker-compose -f docker-compose.test.yml -p timelapse up
```

See logs from tests:
```bash
docker logs -f timelapse_test_1
```

Build and run app:
```bash
docker-compose build
docker-compose up
```

See logs from app:
```bash
docker logs -f timelapse_app_1
```

TODO
* do not be root in docker
* unit tests
* run unit tests automatic on every commit
* give directories as arguments
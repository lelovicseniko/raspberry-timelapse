from pathlib import Path

import pytest

from src.FilePathConfiguration import FilePathConfiguration


class TestFilePathConfiguration:

    EMPTY = ""
    VIDEO_PATH = "/tmp/video"
    IMAGE_PATH = "/tmp/video"
    EXISTING_DIRECTORY = "/home"

    @pytest.mark.parametrize("path", [EMPTY, None])
    def test_empty_video_path(self, path):
        config = None
        with pytest.raises(ValueError) as ex:
            config = FilePathConfiguration(
                image_directory_path=TestFilePathConfiguration.EXISTING_DIRECTORY,
                video_directory_path=path
            )
        assert config is None
        assert str(ex.value) == "Empty video directory path"

import pytest

from src.FtpConfiguration import FtpConfiguration


class TestFtpConfiguration:
    EMPTY = ""
    VALID_HOST = "ftp.zivatar.hu"
    INVALID_HOST = "www.zivatar.hu"
    VALID_USER_NAME = "Bxnti1Ot2FKDT9olAgdM"
    VALID_PASSWORD = "x0oz0eVmRJsfmPMLZ0OC"
    VALID_SOURCE_DIRECTORY = "images"

    @pytest.mark.parametrize("source_directory", [VALID_SOURCE_DIRECTORY, EMPTY])
    def test_happy_path(self, source_directory):
        config = FtpConfiguration(
            host=TestFtpConfiguration.VALID_HOST,
            user_name=TestFtpConfiguration.VALID_USER_NAME,
            password=TestFtpConfiguration.VALID_PASSWORD,
            source_directory=source_directory
        )
        assert (config.host == TestFtpConfiguration.VALID_HOST)
        assert (config.user_name == TestFtpConfiguration.VALID_USER_NAME)
        assert (config.password == TestFtpConfiguration.VALID_PASSWORD)
        assert (config.source_directory == source_directory)

    @pytest.mark.parametrize("password", [EMPTY, None])
    def test_no_password(self, password):
        config = None
        with pytest.raises(ValueError) as ex:
            config = FtpConfiguration(
                host=TestFtpConfiguration.VALID_HOST,
                user_name=TestFtpConfiguration.VALID_USER_NAME,
                password=password,
                source_directory=TestFtpConfiguration.VALID_SOURCE_DIRECTORY
            )
        assert config is None
        assert str(ex.value) == "Empty FTP password"

    @pytest.mark.parametrize("user_name", [EMPTY, None])
    def test_no_user_name(self, user_name):
        config = None
        with pytest.raises(ValueError) as ex:
            config = FtpConfiguration(
                host=TestFtpConfiguration.VALID_HOST,
                user_name=user_name,
                password=TestFtpConfiguration.VALID_PASSWORD,
                source_directory=TestFtpConfiguration.VALID_SOURCE_DIRECTORY
            )
        assert config is None
        assert str(ex.value) == "Empty FTP user name"

    @pytest.mark.parametrize("host", [EMPTY, None])
    def test_missing_host(self, host):
        config = None
        with pytest.raises(ValueError) as ex:
            config = FtpConfiguration(
                host=host,
                user_name=TestFtpConfiguration.VALID_USER_NAME,
                password=TestFtpConfiguration.VALID_PASSWORD,
                source_directory=TestFtpConfiguration.VALID_SOURCE_DIRECTORY
            )
        assert config is None
        assert str(ex.value) == "Empty FTP host"

    def test_invalid_host(self):
        config = None
        with pytest.raises(ValueError) as ex:
            config = FtpConfiguration(
                host='RySA3FnPtiiDKxscLxyH.EwFO3k6SRIh7XPivLJsI',
                user_name=TestFtpConfiguration.VALID_USER_NAME,
                password=TestFtpConfiguration.VALID_PASSWORD,
                source_directory=TestFtpConfiguration.VALID_SOURCE_DIRECTORY
            )
        assert config is None
        assert str(ex.value) == "FTP host does not start with 'ftp'"


FROM python:3.6
ADD . /project
WORKDIR /project
RUN pip install -r requirements.txt
CMD ["python", "app.py"]